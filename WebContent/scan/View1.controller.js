var method = [];
sap.ui.controller("scan.View1", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf scan.View1
*/
	onInit: function() {
		
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf scan.View1
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf scan.View1
*/
	onAfterRendering: function() {
		//sap.ui.getCore().byId("idView11--encodeId").addStyleClass("Hide");
	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf scan.View1
*/
//	onExit: function() {
//
//	}
	oView: null,
    onInit: function() {
        oView = this.getView();
    },
    onBack: function(){
    	app.to("iddashboard")
    },
    onScan: function() {
        var options = {
            preferFrontCamera: false, // iOS and Android
            showFlipCameraButton: true, // iOS and Android
            showTorchButton: true, // iOS and Android
            torchOn: true, // Android, launch with the torch switched on (if available)
            prompt: "Place barcode inside the scan area", // Android
            resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
            formats: "QR_CODE,PDF_417,UPC_A", // default: all but PDF_417 and RSS_EXPANDED
            orientation: "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
            disableAnimations: true // iOS
        };
        cordova.plugins.barcodeScanner.scan(this.onScanSuccess, this.onScanError, options);
    },
    onScanSuccess: function(result) {
    	
    	var oModel = new sap.ui.model.json.JSONModel();
    	var oText = oView.byId("myBarCode");
       // oText.setText("barcode is " + result.text + " and format is " + result.format);
        method.push(result);
		var data = {
				empList : method
					  };
		//alert(JSON.stringify(data));
		//oText.setText(data);
		oModel.setData(data);
		oText.setModel(oModel);
		sap.ui.getCore().setModel(oModel,"ui5empModel");
		
    },
    onScanError: function(error) {
        alert("error" + JSON.stringify(error));
    },
   /* onEncode: function() { 
        cordova.plugins.barcodeScanner.encode(cordova.plugins.barcodeScanner.Encode.TEXT_TYPE, "http://www.nytimes.com", function(success) {
                alert("encode success: " + success);
              }, function(fail) {
                alert("encoding failed: " + fail);
              }
            );
    	},*/
    onEncodePress: function() {
    	app.to("idprintPage");
    },

});