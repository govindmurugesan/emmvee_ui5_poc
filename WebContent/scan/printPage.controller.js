sap.ui.controller("scan.printPage", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf scan.printPage
*/
//	onInit: function() {
//
//	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf scan.printPage
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf scan.printPage
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf scan.printPage
*/
//	onExit: function() {
//
//	}
	onBack: function(){
    	app.to("idView11")
    },
    printpress: function(){
    	//window.print();
		cordova.plugins.printer.isAvailable(
    //Check whether the printer is available or not.
		function (isAvailable) {
			 //Enter the page location.
			var page = document.getElementById('idprintPage--encodeImgId');
			 cordova.plugins.printer.print(page);
		});
    }
});